FROM oraclelinux:8.4

RUN dnf upgrade -y && dnf install -y oracle-epel-release-el8 @php:7.4 httpd php-pecl-zip \
    php-gmp php-ldap php-gd gd php-mysqlnd redis php-pecl-apcu memcached unzip

VOLUME /var/www/html

RUN unzip 